@php
    $establishment = $document->user->establishment;
    $customer = $document->customer;
    $tittle = str_pad($document->id, 8, '0', STR_PAD_LEFT);
@endphp
<html>
<head>
    {{--<title>{{ $tittle }}</title>--}}
    {{--<link href="{{ $path_style }}" rel="stylesheet" />--}}
</head>
<body>
<table class="full-width">
    <tr>
        @if($company->logo)
            <td width="20%">
                <div class="company_logo_box">
                    <img src="data:{{mime_content_type(public_path("storage/uploads/logos/{$company->logo}"))}};base64, {{base64_encode(file_get_contents(public_path("storage/uploads/logos/{$company->logo}")))}}" alt="{{$company->name}}" class="company_logo" style="max-width: 150px;">
                </div>
            </td>
        @else
            <td width="20%">
                {{--<img src="{{ asset('logo/logo.jpg') }}" class="company_logo" style="max-width: 150px">--}}
            </td>
        @endif
        <td width="50%" class="pl-3">
            <div class="text-left">
                <h4 class="">{{ $company->name }}</h4>
                <h5>{{ 'RUC '.$company->number }}</h5>
                <h6 style="text-transform: uppercase;">
                    {{ ($establishment->address !== '-')? $establishment->address : '' }}
                    {{ ($establishment->district_id !== '-')? ', '.$establishment->district->description : '' }}
                    {{ ($establishment->province_id !== '-')? ', '.$establishment->province->description : '' }}
                    {{ ($establishment->department_id !== '-')? '- '.$establishment->department->description : '' }}
                </h6>

                @isset($establishment->trade_address)
                    <h6>{{ ($establishment->trade_address !== '-')? 'D. Comercial: '.$establishment->trade_address : '' }}</h6>
                @endisset
                <h6>{{ ($establishment->telephone !== '-')? 'Central telefónica: '.$establishment->telephone : '' }}</h6>

                <h6>{{ ($establishment->email !== '-')? 'Email: '.$establishment->email : '' }}</h6>

                @isset($establishment->web_address)
                    <h6>{{ ($establishment->web_address !== '-')? 'Web: '.$establishment->web_address : '' }}</h6>
                @endisset

                @isset($establishment->aditional_information)
                    <h6>{{ ($establishment->aditional_information !== '-')? $establishment->aditional_information : '' }}</h6>
                @endisset
            </div>
        </td>
        <td width="30%" class="border-box py-4 px-2 text-center">
            <h5 class="text-center">SERVICIO TÉCNICO</h5>
            <h3 class="text-center">{{ $tittle }}</h3>
        </td>
    </tr>
</table>
<table class="full-width mt-5">
    <tr>
        <td width="15%">Cliente:</td>
        <td width="45%">{{ $customer->name }}</td>
        <td width="25%">Fecha de emisión:</td>
        <td width="15%">{{ $document->date_of_issue->format('Y-m-d') }}</td>
    </tr>
    <tr>
        <td>{{ $customer->identity_document_type->description }}:</td>
        <td>{{ $customer->number }}</td>
        <td width="25%">Fecha de Salida:</td>
        <td width="15%">{{ $document->fechasalida }}</td>
    </tr>
    @if ($customer->address !== '')
    <tr>
        <td class="align-top">Dirección:</td>
        <td colspan="">
            {{ $customer->address }}
            {{ ($customer->district_id !== '-')? ', '.$customer->district->description : '' }}
            {{ ($customer->province_id !== '-')? ', '.$customer->province->description : '' }}
            {{ ($customer->department_id !== '-')? '- '.$customer->department->description : '' }}
        </td>
    </tr>
    @endif
    
    <tr>
        <td class="align-top">kILOMETRAGE:</td>
        <td colspan="3">
            {{ $document->serial_number }}
        </td>
    </tr>

</table>
<table  class="full-width mt-2">
    <tr>
        <td> <input {{ $document->repair ? 'checked="checked"' : '' }} class="tech-chk" type="checkbox"> Reparación </td>
        <td> <input {{ $document->maintenance ? 'checked="checked"' : '' }}  class="tech-chk" type="checkbox"> Mantenimiento </td>
        <td> <input {{ $document->warranty ? 'checked="checked"' : '' }}  class="tech-chk" type="checkbox"> Garantía </td>
        <td> <input {{ $document->diagnosis ? 'checked="checked"' : '' }}  class="tech-chk" type="checkbox"> Diagnostico </td>
    </tr>
</table>

<table class="full-width mt-3">
    <tr>
        <td ><b>Descripción:</b></td>
    </tr>
    <tr>
        <td>{{ $document->description }}</td>
    </tr>
   
    @if($document->activities)
    <tr>
        <td><b>Actividades realizadas:</b> {{ $document->activities }} </td>
    </tr>
    @endif
    <tr>
        <td><b>Placa:</b> {{ $document->brand }} </td>
    </tr>
    </table>
    <table border="1" class="full-width mt-3">
    <tr>
        <td colspan="4" ><b>REPARACIONES EJECUTADAS EN GENERAL:</b>  </td>
    </tr>
    <tr>
    
        <td style="padding-top: 7px;"><b>FECHA</b> </td>
        <td style="padding-top: 7px;"><b>DESCRIPCION </b></td>
        <td style="padding-top: 7px;"><b>MECANICO </b> </td>      
        <td style="padding-top: 7px;"><b>PRECIO </b></td>         
                     
                   
            
       
    
    </tr>
    <tr>
    @php 
    $total2=0;
    @endphp
    @foreach($document->important_note as $note)
        <td style="padding-top: 7px;">{{ $note->fecha}}  </td>
        <td style="padding-top: 7px;">{{ $note->description }}  </td>
        <td style="padding-top: 7px;">{{ $note->mecanico }}  </td>      
        <td style="padding-top: 7px;">{{ $note->precio }}  </td>         
                     
                   
        @php 
        $total2=$total2+$note->precio;
    @endphp
            
       
        @endforeach
    </tr>
    <tr>
    
        <td style="padding-top: 7px;"><b>Total:  {{$total2}} </b> </td>
       
    </tr>
    </table>
    <table border="1" class="full-width mt-3">
    <tr>
        <td colspan="5" ><b> REPUESTOS IMPLEMENTADOS:</b>  </td>
    </tr>
    <tr>
    
        <td style="padding-top: 7px;"><b>FECHA</b> </td>
        <td style="padding-top: 7px;"><b>CANT </b></td>
        <td style="padding-top: 7px;"><b>UND.MEDIDA </b> </td>      
        <td style="padding-top: 7px;"><b>DENOMINACION </b></td>         
        <td style="padding-top: 7px;"><b>IMPORTE </b></td>             
                   
            
       
    
    </tr>
    <tr>
    @php 
    $total=0;
    @endphp
    @foreach($document->important_piesas as $note)
        <td style="padding-top: 7px;">{{ $note-> fecha}}  </td>
        <td style="padding-top: 7px;">{{ $note->cantidad }}  </td>
        <td style="padding-top: 7px;">{{ $note->undmedida }}  </td>      
        <td style="padding-top: 7px;">{{ $note->description }}  </td>         
        <td style="padding-top: 7px;">{{ $note->importe }}  </td>           
                   
       
        @php 
        $total=$total+$note->importe;
    @endphp
       
        @endforeach
    </tr>
    <tr>
    
        <td style="padding-top: 7px;"><b>Total:  {{$total}} </b> </td>
       
    </tr>
</table>


<table class="full-width">
        <tr>
            <td colspan="4" class="text-right font-bold mb-3">COSTO DEL SERVICIO: </td>
            <td class="text-right font-bold">{{ number_format($document->cost, 2) }}</td>
        </tr>
        <tr>
            <td colspan="4" class="text-right font-bold">PAGO ADELANTADO: </td>
            <td class="text-right font-bold">{{ number_format($document->prepayment, 2) }}</td>
        </tr>
        <tr>
            <td colspan="4" class="text-right font-bold">SALDO A PAGAR: </td>
            <td class="text-right font-bold">{{ number_format($document->cost - $document->prepayment, 2) }}</td>
        </tr>
</table>

<table class="full-width" style="margin-top:80px">
        <tr>
            <td colspan="2" class="text-right font-bold mb-1">____________________</td>
            <td class="text-right font-bold"></td>
            <td colspan="2" class="text-right font-bold mb-1">____________________</td>
            <td class="text-right font-bold"></td>
            <td colspan="2" class="text-right font-bold mb-1">____________________</td>
            <td class="text-right font-bold"></td>
        </tr>
        <tr>
            <td colspan="2" class="text-right font-bold mb-1">CONDUCTOR</td>
            <td class="text-right font-bold"></td>
            <td colspan="2" class="text-right font-bold mb-1">MECANICO</td>
            <td class="text-right font-bold"></td>
            <td colspan="2" class="text-right font-bold mb-1">ALMACEN</td>
            <td class="text-right font-bold"></td>
        </tr>
        
</table>

{{-- MITAD --}}


</body>
</html>
