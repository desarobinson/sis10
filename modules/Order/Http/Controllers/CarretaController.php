<?php
namespace Modules\Order\Http\Controllers;

use Modules\Order\Http\Requests\CarretaRequest;
use Modules\Order\Http\Resources\CarretaCollection;
use Modules\Order\Http\Resources\CarretaResource;
use App\Models\Tenant\Catalogs\IdentityDocumentType;
use App\Http\Controllers\Controller;
use Modules\Order\Models\Carreta;
use Illuminate\Http\Request;

class CarretaController extends Controller
{

    public function index()
    {
        return view('order::carretas.index');
    }

    public function columns()
    {
        return [
            'placa' => 'placa',
            'modelo' => 'modelo',
        ];
    }

    public function records(Request $request)
    {

        $records = Carreta::where($request->column, 'like', "%{$request->value}%")
                            ->orderBy('placa');

        return new CarretaCollection($records->paginate(config('tenant.items_per_page')));
    }


    public function tables()
    {
        $identity_document_types = IdentityDocumentType::whereActive()->get();
        $api_service_token = config('configuration.api_service_token');

        return compact('identity_document_types', 'api_service_token');
    }

    public function record($id)
    {
        $record = new CarretaResource(Carreta::findOrFail($id));

        return $record;
    }

    public function store(CarretaRequest $request)
    {

        $id = $request->input('id');
        $record = Carreta::firstOrNew(['id' => $id]);
        $record->fill($request->all());
        $record->save();

        return [
            'success' => true,
            'message' => ($id)?'Carreta editado con éxito':'Carreta registrado con éxito',
            'id' => $record->id
        ];
    }

    public function destroy($id)
    {

        $record = Carreta::findOrFail($id);
        $record->delete();

        return [
            'success' => true,
            'message' => 'Carreta eliminado con éxito'
        ];

    }

}
